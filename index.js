require('dotenv').config()
const {
		Configuration,
		OpenAIApi
} = require("openai");

const {
		MatrixClient,
		AutojoinRoomsMixin,
		SimpleFsStorageProvider,
} = require("matrix-bot-sdk");

// env vars contants

const {
		MATRIX_HOMESERVER_URL = 'https://matrix.org',
		MATRIX_ACCESS_TOKEN = '',
		MATRIX_USER_ID = '',
		MATRIX_ROOMS_ID_TO_JOIN = '',
		MATRIX_ROOMS_ID_TO_IGNORE = '',
		OPENAI_API_KEY = '',
} = process.env

// openai
const openAIApiKey = OPENAI_API_KEY

// Set up the Matrix client
const homeserverUrl = MATRIX_HOMESERVER_URL
const matrixAccessToken = MATRIX_ACCESS_TOKEN
const userId = MATRIX_USER_ID

// In order to make sure the bot doesn't lose its state between restarts, we'll give it a place to cache
const storage = new SimpleFsStorageProvider("matrix-bot-storage.json")

const client = new MatrixClient(homeserverUrl, matrixAccessToken, storage);

AutojoinRoomsMixin.setupOnClient(client)

// Set up the OpenAI client
const configuration = new Configuration({
		apiKey: openAIApiKey,
})
const openai = new OpenAIApi(configuration)

// Define the rooms that the bot will listen to
const roomsToJoin = MATRIX_ROOMS_ID_TO_JOIN.split(',').filter(r => !!r)
const roomsToIgnore = MATRIX_ROOMS_ID_TO_IGNORE.split(',').filter(r => !!r)

// Function to send a message to a room
async function sendMessage(roomId, message) {
		if (!message) return

		await client.sendMessage(roomId, {
				"msgtype": "m.text",
				"body": message,
		})
}

const modelOptions = {
		/* temperature: 0.5, */
		max_tokens: 500,
		/* n: 1, */
		/* stop: '\n', */
}

// Function to generate a response using the OpenAI API
async function generateResponse(input) {
		const completion = await openai.createCompletion({
				model: "text-davinci-003",
				prompt: input,
				...modelOptions,
		})
		console.log(completion.data.error)
		return completion.data.choices[0].text.trim()
}

async function handleMessage(roomId, event) {
		console.log('message', roomId, event.content.msgtype)

		if (roomsToJoin.length && !roomsToJoin.includes(roomId)) return
		if (roomsToIgnore.length && roomsToIgnore.includes(roomId)) return

		// Don't handle unhelpful events (ones that aren't text messages, are redacted, or sent by us)
		if (event['content']?.['msgtype'] !== 'm.text') return
		if (event['sender'] === await client.getUserId()) return
		if (event.sender === userId) return

		console.log('handled message', roomId, event.content.body)

		// Generate a response to the message using the OpenAI API
		let response
		try {
				response = await generateResponse(event.content.body)
		} catch (e) {
				return console.error('error generating openAI response', e.response)
		}

		console.log('response', response)

		// Send the response to the room
		await sendMessage(roomId, response)
}

// Listen for messages in the selected rooms
client.on("room.message", handleMessage)


// Join the selected rooms
async function joinRooms() {
		if (!roomsToJoin.length) return
		for (const room of roomsToJoin) {
				await client.joinRoom(room)
		}
}

// Start the bot
(async () => {
		await joinRooms()
		await client.start()
		console.log("started matrix-openai-bot")
})();
