> very alpha version; just testing what this project can do

This project is using openAI's chatGPT to answer a matrix,org user's
messages. It should also answer messages receive from matrix bridges
(signal, whatsapp, telegram etc.), such as when used with a [Element
Home](https://element.io/blog/element-home/) matrix account.

By default, it will answer to all messsages, in all rooms; so take care with which matrix account you are using, or you will annoy people with AI_BOT_SPAM™

> Best create a dedicated Matrix user account for your bot, or use matrix element-home if you want to have the bridges working to answer all your conversations (that's where the fun's at though).

# Usage

0. Clone this repository, navigate into it, and open a `shell`.

1. To use this project, you will need to install `node.js` on your
computer (it can be your local computer, or a remote server).

2. Install the node.js package dependencies with `npm install`

3. Copy the `.env.example` file to a `.env` file
4. Update the `.env` file values with the correct ENV variables values

- `OPENAI_API_KEY`: https://platform.openai.com/account/api-keys
- `MATRIX_ACCESS_TOKEN`: find it in ` element > all settings > help about > advanced > access token`
- `MATRIX_HOMESERVER_URL` the homeserver where your user is registered; find it in ` element > all settings > help about > advanced > homeserver`
- `MATRIX_USER_ID`, your user matrix user ID (example: `@user:domain.tld`)

3. Run the script bot's script with `node index.js`

4. Stop the script with the keyboard shortcurt `C-c` (control + `c` letter) (interupt signal)

# Docs
- https://platform.openai.com/docs/api-reference
- https://turt2live.github.io/matrix-bot-sdk
- https://www.npmjs.com/package/dotenv
